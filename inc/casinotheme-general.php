<?php

/**
 * General functions
 *
 * @package Kentaurus
 */

/**
 * Register and Enqueue Styles.
 */

function kentaurus_register_styles()
{
    $css_path = KENTAURUS_TEMPLATE_DIR . '/dist/css/style.css';

    if (DEVELOPMENT) {
        wp_enqueue_style('kentaurus-style', KENTAURUS_DIR_URI . '/dist/css/style.css', array(), filemtime($css_path));
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap');
    } else {
        wp_enqueue_style('kentaurus-style', KENTAURUS_DIR_URI . '/dist/css/style.css', array(), KENTAURUS_VER);      
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap');
    }
}

add_action('wp_enqueue_scripts', 'kentaurus_register_styles');

/**
 * Register and Enqueue Scripts.
 */
function kentaurus_register_scripts()
{
    $js_path = KENTAURUS_TEMPLATE_DIR . '/dist/js/scripts.js';

    if (DEVELOPMENT) {
        wp_enqueue_script('kentaurus-scripts', KENTAURUS_DIR_URI . '/dist/js/scripts.js', array('jquery'), filemtime($js_path), false);
        wp_script_add_data('kentaurus-scripts', 'async', true);
        wp_localize_script('kentaurus-scripts', 'wp_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
    } else {
        wp_enqueue_script('kentaurus-scripts', KENTAURUS_DIR_URI . '/dist/js/scripts.js', array('jquery'), KENTAURUS_VER, false);
        wp_script_add_data('kentaurus-scripts', 'async', true);
        wp_localize_script('kentaurus-scripts', 'wp_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
    }
}

add_action('wp_enqueue_scripts', 'kentaurus_register_scripts');

function kentaurus_register_admin_styles()
{
    wp_enqueue_style('kentaurus-admin-style', KENTAURUS_DIR_URI . '/dist/css/admin-style.css', array(), KENTAURUS_VER);
}

add_action('admin_enqueue_scripts', 'kentaurus_register_admin_styles');
