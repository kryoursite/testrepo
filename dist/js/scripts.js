jQuery(document).ready(function ($) {
  // Main menu - add icon to items with sub-menu
  var $menu_item = $('.main-menu .menu-item-has-children');
  $menu_item.append('<i class="arrow arrow-down arrow-main-menu"></i>');

  $menuWithChildren = $(".main-menu .menu-item-has-children");
  //Show submenu on mouseenter
  $menuWithChildren.on("mouseenter", function () {
    $(".sub-menu", this).show();
  });

  //Hide submenu on mouseleave
  $menuWithChildren.on("mouseleave", function () {
    $(".sub-menu", this).hide();
  });

  //jQuery function for showing submenu items in the sidenav/mobile menu
  $(".mobile-menu .menu-item-has-children").on("click", function () {
    $(".sub-menu", this).slideToggle();
  });

if ( $(window).width() <= 599 ) {
$("#nav-icon").on("click", function () {
$("#sidenav").toggleClass("enter");
$(".hamburger-menu").toggleClass("open");
$("body").toggleClass("overflow-hidden");
});
//Close mobile-menu if clicked on body
$(document).click(function(){
$("#sidenav").removeClass("enter");
$(".hamburger-menu").removeClass("open");
$("body").removeClass("overflow-hidden");
});

$("#sidenav, .hamburger-menu, .side__nav__logo , #close-button").click(function(e){
 e.stopPropagation(); 
});
}else{
$("#nav-icon").on("click", function () {
$("#sidenav").toggleClass("enter");
$(".hamburger-menu").toggleClass("open");
$(".side__nav__logo").toggleClass("enter");
$("#close-button").toggleClass("open");
});
$("#close-button").on("click", function () {
$("#sidenav").removeClass("enter");
$(".hamburger-menu").removeClass("open");
$(".side__nav__logo").removeClass("enter");    
$("#close-button").removeClass("open");  
});
//Close menu if clicked on body
$(document).click(function(){
$("#sidenav").removeClass("enter");
$(".hamburger-menu").removeClass("open");
$(".side__nav__logo").removeClass("enter");    
$("#close-button").removeClass("open");  
});

$("#sidenav, .hamburger-menu, .side__nav__logo , #close-button").click(function(e){
 e.stopPropagation(); 
});
};

// Back to Top button 
var btn = $('#back-to-top-button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 150) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

   //Open / close language switcher with click
   $(".fa-globe").on("click", function () {
    $(".wpml-ls-legacy-dropdown").toggle("slow");

  });

  //Open search bar
  $(".site-header__search").on("click", function () {
    $(".search-bar").slideToggle();
  });

  //Front page nav
  $(".header-block-more span").on("click", function () {
    $(".front-nav").slideToggle();
  });

  //Show mobile navigation
  $(".inner-mobile-navigation span").on("click", function () {
    $(".inner-mobile-navigation ul").slideToggle();
  });

  //Casino page show licence
  $(".licence-hover").on("mouseenter", function () {
    $(".licence-text").show();
  });

  //Casino page hide licence
  $(".licence-hover").on("mouseleave", function () {
    $(".licence-text").hide();
  });

  //Casino page show all payment methods
  $(".payment-options .see-more").on("mouseenter", function () {
    $(".all-payments").css("display", "flex");
  });

  $(".payment-options .see-more").on("mouseleave", function () {
    $(".all-payments").css("display", "none");
  });

  //Casino page show all software providers
  $(".software .see-more").on("mouseenter", function () {
    $(".all-software").css("display", "flex");
  });

  $(".software .see-more").on("mouseleave", function () {
    $(".all-software").css("display", "none");
  });

  //Check if page menu has children. If no hide page nav menu block
  var pageMenuList = $('.page-nav-list');
  if (pageMenuList.children().length < 1) {
    $('.inner-page-navigation').hide();
  }

  // Ajax load more button
  page = 2;
  ppp = 18;

  $(".load-more").on("click", function (e) {
    e.preventDefault();
    var button = $(this);
    $.ajax({
      url: wp_ajax_object.ajax_url,
      data: {
        action: "kentaurus_load_more_posts", // add your action to the data object
        page: page,
        ppp: ppp
      },

      beforeSend: function () {
        button.prop('disabled', true);
      },

      success: function (data) {
        page++;
        $('.inner-archive-content').append(data);
        button.prop('disabled', false);

      },
      error: function (data) {
        // test to see what you get back on error
        console.log(data);
      }
    });
  });

  // Ajax load casino categories button
  $(".button-filter").on("click", function (e) {

    e.preventDefault();
    category = $(this).data('category');

    $.ajax({
      url: wp_ajax_object.ajax_url,
      data: {
        action: "kentaurus_filter_categories", // add your action to the data object
        category: category,
      },
      type: 'POST',

      beforeSend: function () {
        $("#loader").show();
        $(".casino-list-item").hide();
      },

      success: function (data) {

        $('.casino-list').html(data);

      },

      complete: function (data) {
        $("#loader").hide();
      },


      error: function (data) {
        // test to see what you get back on error
        console.log(data);
      }
    });
  });
});

(function ($) {
  "use strict";

  // Builds a list with the table of contents in the current selector.
  // options:
  //   content: where to look for headings
  //   headings: string with a comma-separated list of selectors to be used as headings, ordered
  //   by their relative hierarchy level
  var toc = function (options) {
    return this.each(function () {
      var root = $(this),
        data = root.data(),
        thisOptions,
        stack = [root], // The upside-down stack keeps track of list elements
        listTag = this.tagName,
        currentLevel = 0,
        headingSelectors;

      // Defaults: plugin parameters override data attributes, which override our defaults
      thisOptions = $.extend(
        { content: "body", headings: "h1,h2,h3" },
        { content: data.toc || undefined, headings: data.tocHeadings || undefined },
        options
      );
      headingSelectors = thisOptions.headings.split(",");

      // Set up some automatic IDs if we do not already have them
      $(thisOptions.content).find(thisOptions.headings).attr("id", function (index, attr) {
        // In HTML5, the id attribute must be at least one character long and must not
        // contain any space characters.
        //
        // We just use the HTML5 spec now because all browsers work fine with it.
        // https://mathiasbynens.be/notes/html5-id-class
        var generateUniqueId = function (text) {
          // Generate a valid ID. Spaces are replaced with underscores. We also check if
          // the ID already exists in the document. If so, we append "_1", "_2", etc.
          // until we find an unused ID.

          if (text.length === 0) {
            text = "?";
          }

          var baseId = text.replace(/\s+/g, "_"), suffix = "", count = 1;

          while (document.getElementById(baseId + suffix) !== null) {
            suffix = "_" + count++;
          }

          return baseId + suffix;
        };

        return attr || generateUniqueId($(this).text());
      }).each(function () {
        // What level is the current heading?
        var elem = $(this), level = $.map(headingSelectors, function (selector, index) {
          return elem.is(selector) ? index : undefined;
        })[0];

        if (level > currentLevel) {
          // If the heading is at a deeper level than where we are, start a new nested
          // list, but only if we already have some list items in the parent. If we do
          // not, that means that we're skipping levels, so we can just add new list items
          // at the current level.
          // In the upside-down stack, unshift = push, and stack[0] = the top.
          var parentItem = stack[0].children("li:last")[0];
          if (parentItem) {
            stack.unshift($("<" + listTag + "/>").appendTo(parentItem));
          }
        } else {
          // Truncate the stack to the current level by chopping off the 'top' of the
          // stack. We also need to preserve at least one element in the stack - that is
          // the containing element.
          stack.splice(0, Math.min(currentLevel - level, Math.max(stack.length - 1, 0)));
        }

        // Add the list item
        $("<li/>").appendTo(stack[0]).append(
          $("<a/>").text(elem.text()).attr("href", "#" + elem.attr("id"))
        );

        currentLevel = level;
      });
    });
  }, old = $.fn.toc;

  $.fn.toc = toc;

  $.fn.toc.noConflict = function () {
    $.fn.toc = old;
    return this;
  };

  // Data API
  $(function () {
    toc.call($("[data-toc]"));
  });
}(window.jQuery));
