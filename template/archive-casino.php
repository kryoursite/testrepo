<?php

/**
 * The template for displaying casino archive page
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<main class="site-main" role="main">
    <div class="casino-archive container">
        <?php if (get_field('casino_archive_page_title', 'options')) : ?>
            <h1 class="my-20">
                <?php the_field('casino_archive_page_title', 'options'); ?>
            </h1>
        <?php else : ?>
            <h1 class="my-20">Casino Archive Page</h1>
        <?php endif; ?>
        <div class="inner-casino-archive">
            <?php the_field('casino_archive_page_conent', 'options'); ?>
        </div>
    </div>
</main>