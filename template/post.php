<?php

/**
 * The template for displaying posts.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<main class="site-main" role="main">
    <div class="post-content">
        <div class="inner-post-content container">
            <?php while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        </div>
    </div>
    <?php get_template_part('template-parts/author-box'); ?>
</main>