<?php

/**
 * Simple page header block
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list-item base-style" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="margin-bottom:70px;"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list__order"></span>
    <div class="casino-list__logo">
        <a href="<?php the_field('casino_redirect_link'); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
    </div>
    <div class="casino-list__bonus">
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <?php the_field('casino_sportsbook_line'); ?>
        <?php else : ?>
            <?php the_field('casino_toplist_bonus_line'); ?>
        <?php endif; ?>
    </div>
    <div class="casino-list__rating">
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <span class="casino-name"><?php the_field('casino_extra_rating_text'); ?></span>
        <?php else : ?>
            <span class="casino-name"><?php the_title(); ?></span>
        <?php endif; ?>
        <div class="star-rating"><i class="star"></i></div>
        <div class="casino-rating">
            <?php if (get_field('casino_toplist_rating')) : ?>
                <?php the_field('casino_toplist_rating'); ?>
            <?php else : ?>
                <?php echo '-'; ?>
            <?php endif; ?>

        </div>
    </div>
    <div class="casino-list__pluses">
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <?php
            if (have_rows('sportsbook_top_3')) :
                while (have_rows('sportsbook_top_3')) : the_row(); ?>
                    <span class="plus"><i class="fas fa-check"></i><?php the_sub_field('top_three_line'); ?></span>
            <?php
                endwhile;
            endif;
            ?>
        <?php else : ?>
            <?php
            if (have_rows('casino_top_3')) :
                while (have_rows('casino_top_3')) : the_row(); ?>
                    <span class="plus"><i><span class="checkmark">
                            <div class="checkmark_stem"></div>
                            <div class="checkmark_kick"></div>
                    </span></i><?php the_sub_field('top_three_line'); ?></span>
            <?php
                endwhile;
            endif;
            ?>
        <?php endif; ?>
    </div>
    <div class="casino-list__more">
        <a href="<?php the_permalink(); ?>" class="list-review"><?php _e('Casino Review', 'kentaurus'); ?> <i class="arrow arrow-right"></i></a>
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <a href="<?php the_field('sportsbook_affiliate_link'); ?> " rel="nofollow noopener" target="_blank"  class="to-the-casino"><?php _e('Get Bonus!', 'kentaurus'); ?></a>
        <?php elseif ($atts['type'] == 'casino') : ?>
            <a href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank" class="to-the-casino"><?php _e('Get Bonus!', 'kentaurus'); ?></a>
        <?php endif; ?>
    </div>
    <?php if (get_field('enable_terms_and_conditions')) : ?>
        <div class="casino-list__terms">
            <i class="fas fa-info-circle"></i><span><?php the_field('terms_and_conditions'); ?></span>
        </div>
    <?php endif; ?>
</div>