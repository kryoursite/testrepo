<div class="casino-header-inner container">
    <div class="casino-information">
        <div class="casino-info-inner">
            <div class="breadcrumbs">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            </div>
            <h1 class="casino-title">
                <?php the_title(); ?>
            </h1>
            <span class="casino-modified">
                <?php _e('Last updated:', 'kentaurus'); ?> <?php echo get_the_modified_date("d/m/Y"); ?>
            </span>
        </div>
    </div>
    <div class="top-header">
        <div class="bonus-part">

            <div class="bonus-part-top p-20">
                <a  href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank" ><?php the_post_thumbnail(); ?></a>

                <a href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank" class="bonus-text"><?php the_field('casino_bonus_line'); ?></a>
            </div>
            <a href="<?php the_field('casino_redirect_link'); ?> " rel="nofollow noopener" target="_blank" class="get-bonus p-10">
                <?php _e('Get Bonus', 'kentaurus'); ?>
            </a>
            </a>
        </div>
        <div class="general-information p-10">
            <table>
                <caption><i class="fas fa-info-circle"></i> <?php _e('General Information', 'kentaurus'); ?></caption>
                <tbody>
                    <tr>
                        <th scope="row"><span class="casino-emoji">📅</span> <?php _e('Year', 'kentaurus'); ?></th>
                        <td><?php the_field('casino_year'); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">📃</span> <?php _e('Licence', 'kentaurus'); ?></th>
                        <td class="licence-info"><span class="licence-hover">&#x2139;</span>
                            <p class="licence-text"><?php the_field('casino_licence'); ?></p>
                        </td>
                    </tr>

                    <tr>
                        <th scope="row"><span class="casino-emoji">📧</span> <?php _e('Email', 'kentaurus'); ?></th>
                        <td class="general-email"><?php the_field('casino_email'); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">💶</span> <?php _e('Minimal deposit', 'kentaurus'); ?></th>
                        <td><?php the_field('casino_min_deposit'); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">⌚</span> <?php _e('Payout Time', 'kentaurus'); ?></th>
                        <td><?php the_field('casino_payout_time'); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="bonus-information p-10">
            <table>
                <caption><i class="fas fa-gift"></i> <?php _e('Bonus Information', 'kentaurus'); ?></caption>
                <tbody>
                    <tr>
                        <th scope="row"><span class="casino-emoji">💲</span> <?php _e('Bonus', 'kentaurus'); ?></th>

                        <?php if (get_field('bonus_amount')) : ?>
                            <td class="bonus-td"><a href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank "><?php the_field('bonus_amount'); ?></a></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>

                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">✔️</span> <?php _e('Bonus Percentage', 'kentaurus'); ?></th>

                        <?php if (get_field('bonus_percentage')) : ?>
                            <td><?php the_field('bonus_percentage'); ?></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>

                    </tr>

                    <tr>
                        <th scope="row"><span class="casino-emoji">💱</span> <?php _e('Turnover', 'kentaurus'); ?></th>

                        <?php if (get_field('bonus_turnover')) : ?>
                            <td><?php the_field('bonus_turnover'); ?></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>

                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">🎰</span> <?php _e('Freespins', 'kentaurus'); ?></th>
                        <?php if (get_field('bonus_freespins')) : ?>
                            <td><?php the_field('bonus_freespins'); ?></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">📜</span> <?php _e('Bonus Code', 'kentaurus'); ?></th>
                        <?php if (get_field('bonus_code')) : ?>
                            <td><a class="bonus-code" href="<?php the_field('casino_redirect_link'); ?>"><?php the_field('bonus_code'); ?></a></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="bottom-header">
        <div class="bottom-header-inner">
            <?php
            $payments = get_field('payment_options');
            if ($payments) : ?>
                <div class="payment-options">
                    <span class="payments-title"><i class="fas fa-credit-card"></i> <?php _e('Payment Options', 'kentaurus'); ?></span>
                    <div class="payment-images p-10">
                        <?php
                        for ($i = 0; $i < 5; $i++) {
                        ?>
                            <?php if ($payments[$i]) : ?>
                                <div class="payment-img-wrap">
                                    <img src='<?php echo KENTAURUS_DIR_URI . "/dist/img/payments/{$payments[$i]}.png"; ?>' alt="<?php echo $payments[$i]; ?>">
                                </div>
                            <?php endif; ?>
                        <?php
                        }

                        ?>
                        <?php if (count($payments) > 5) : ?>
                            <span class="see-more"><?php _e('See All', 'kentaurus'); ?> <i class="fas fa-chevron-down"></i></span>

                            <div class="all-payments">
                                <?php foreach ($payments as $payment) : ?>
                                    <div class="payment-img-wrap">
                                        <img src='<?php echo KENTAURUS_DIR_URI . "/dist/img/payments/{$payment}.png"; ?>' alt="<?php echo $payments; ?>">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $software = get_field('software_options');
            if ($software) : ?>
                <div class="software">
                    <span class="software-title"><i class="fab fa-uncharted"></i> <?php _e('Software', 'kentaurus'); ?></span>

                    <div class="software-images p-10">
                        <?php
                        for ($i = 0; $i < 5; $i++) {
                        ?>
                            <?php if ($software[$i]) : ?>
                                <div class="software-img-wrap">
                                    <img src='<?php echo KENTAURUS_DIR_URI . "/dist/img/software/{$software[$i]}.png"; ?>' alt="<?php echo $software[$i]; ?>">
                                </div>
                            <?php endif; ?>
                        <?php
                        }

                        ?>
                        <?php
                        if (count($software) > 5) : ?>
                            <span class="see-more"><?php _e('See All', 'kentaurus'); ?> <i class="fas fa-chevron-down"></i></span>
                            <div class="all-software">

                                <?php foreach ($software as $soft) : ?>
                                    <div class="software-img-wrap">
                                        <img src='<?php echo KENTAURUS_DIR_URI . "/dist/img/software/{$soft}.png"; ?>' alt="<?php echo $soft; ?>">
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
