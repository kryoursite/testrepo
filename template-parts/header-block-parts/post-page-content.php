<?php

/**
 * Post page block content part
 *
 * @package kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="page-title">
    <h1><?php the_title(); ?></h1>
</div>
<div class="page-information">
    <div class="breadcrumbs">
        <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
    </div>
</div>
<?php if (get_field('enable_page_header_block_text')) : ?>
    <div class="header-block-text my-10">
        <?php the_field('page_header_block_text'); ?>
    </div>
<?php endif; ?>