<?php

/**
 * Header navigation template part
 * 
 */

?>

<div id="wrapper">
    <a id="back-to-top-button"><span class="arrow arrow-up"></span></a>
        <div id="sidenav-logo"class="side__nav__logo">
                    <div><?php the_custom_logo(); ?></div>
                    <div id="close-button" class="close__button toggle-button">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
    <div id="sidenav" class="sidenav" <?php kentaurus_sidenav_margin(); ?>>
        <?php
        $args = array(
            'theme_location' => 'main-menu',
            'container' => 'nav',
            'container_class' => 'mobile-menu',
            'after'    => '<span class="sub-arrow"><i class="arrow arrow-down arrow-ham-menu"></i></span>',
        );
        wp_nav_menu($args);
        ?>
    </div>

    <header id="site-header" class="site-header" role="banner">
        <div class="header-inner container">
            <div id="nav-icon" class="hamburger-menu toggle-button">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="site-header__logo mx-20">
                <?php the_custom_logo(); ?>
            </div>
            <div class="site-header__nav">
                <?php
                $args = array(
                    'theme_location' => 'main-menu',
                    'container' => 'nav',
                    'container_class' => 'main-menu mx-20',
                );
                wp_nav_menu($args);
                ?>
            </div>
            <div class="site-header__search">
            <i class="search-button">
                    <div class="search__circle"></div>
                    <div class="search__rectangle"></div>
                </i>
            </div>
        </div>
    </header>

    <div class="search-bar">
        <div class="inner-search container">
            <?php get_search_form(); ?>
        </div>
    </div>