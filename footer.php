<?php

/**
 * Footer Template
 * 
 */

?>

<?php wp_footer(); ?>
<footer id="footer" class="footer">
    <div class="inner-footer container py-20">
        <div class="footer-upper">
            <div class="footer-menus">
                <?php if (has_nav_menu("footer-menu-1")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-1"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-2")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-2"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-3")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-3"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-4")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-4"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-5")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-5"); ?>
                <?php endif; ?>
            </div>
            <div class="footer-logos my-20">

                <?php if (have_rows('footer_logos', 'options')) : ?>

                    <?php while (have_rows('footer_logos', 'options')) : the_row();
                        $image = get_sub_field('footer_logo', 'options');
                        $img_link = get_sub_field('footer_enable_img_link', 'options');
                        $link = get_sub_field('footer_logo_link', 'options');
                    ?>
                        <?php if ($img_link) : ?>
                            <a href="<?php echo $link; ?>" class="footer-logo-wrap">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        <?php else : ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                        <?php endif; ?>
                    <?php endwhile; ?>

                <?php endif; ?>
            </div>
        </div>
        <div class="footer-lower">
            <?php if (get_field('footer_info_text', 'options')) : ?>
                <?php the_field('footer_info_text', 'options'); ?>

        <div class="footer-custom">  
            <div class="footer-lower__language">
                <i class="fa fa-globe fa-2x"></i>
                <?php do_action('wpml_add_language_selector');  ?>  
            </div>
            <div class="footer-logo">
                 <?php the_custom_logo(); ?>
            </div> 
        </div> 
           
           
         
            <?php endif; ?>
            <div class="footer-socials">
                <?php if (get_field('social_twitter', 'options')) : ?>
                    <a href="<?php echo get_field('social_twitter', 'options'); ?>"><i class="fab fa-twitter"></i></a>
                <?php endif; ?>
                <?php if (get_field('social_instagram', 'options')) : ?>
                    <a href="<?php echo get_field('social_instagram', 'options'); ?>"><i class="fab fa-instagram"></i></a>
                <?php endif; ?>
                <?php if (get_field('social_facebook', 'options')) : ?>
                    <a href="<?php echo get_field('social_facebook', 'options'); ?>"><i class="fab fa-facebook"></i></a>
                <?php endif; ?>
            </div>
        </div>
        
    </div>
    <div class="footer-copyright">
        <div class="inner-copyright container py-10">
            <span class="footer-date">
                <?php echo date("Y") . " "; ?><?php _e("© All Rights Reserved", 'kentaurus'); ?>
            </span>
        </div>
    </div>
</footer>
</div> <!-- .wrapper end -->
</body>

</html>