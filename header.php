<?php

/**
 * Header Template
 * 
 * @package Kentaurus
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php

    wp_body_open();

    get_template_part('template-parts/navigation');

    if (is_singular('page') || is_singular('post')) {
        //If is simple page get simple header block
        get_template_part('template-parts/header-block');
    }
